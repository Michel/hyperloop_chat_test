module Operations
  # encapsulates our "messages store" server side
  # the params and dispatcher will be inherited
  class ServerBase < Hyperloop::ServerOp
    param :acting_user, nils: true
    param :user_name
    dispatch_to { Hyperloop::Application }

    def messages
      Message.all
    end

    def add_message
      Message.create message: params.message, from: params.user_name
    end
  end

  # get all the messages
  class GetMessages < ServerBase
    outbound :messages

    step { params.messages = messages }

    def self.serialize_response(messages)
      {messages: messages.map(&:attributes)}
    end
  end

  # send a message to everybody
  class Send < ServerBase
    param :message

    step :add_message

    def self.serialize_response(message)
      {message: message.attributes}
    end
  end

  # client side only: registers user_name and then gets the messages
  class Join < Hyperloop::Operation
    param :user_name
    step GetMessages
  end
end